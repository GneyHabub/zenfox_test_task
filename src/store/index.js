import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    pages: {},
    currentPage: 1,
    currentUserList: [],
    totalUsers: null,
    totalPages: null,
    currentTODOs: [],
    currentUser: null
  },
  mutations: {
    setCurrentUserList(state, { list, page }) {
      state.currentUserList = list;
      state.currentPage = page;
    },
    setTotalUsers(state, num) {
      state.currentUserList = num;
      state.totalPages = Math.ceil(num / 5);
    },
    setCurrentTODOs(state, { user, todos }) {
      state.currentUser = user;
      state.currentTODOs = todos;
    },
    updatePages(state, { page, data }) {
      state.pages[page] = data;
    }
  },
  actions: {
    async fetchUsers({ commit, state }, page) {
      if (!state.pages[page]) {
        let users = {};
        try {
          users = await axios.get(
            `https://jsonplaceholder.typicode.com/users?_page=${page}&_limit=5`
          );
        } catch (e) {
          console.error(e);
        }
        commit("updatePages", { page: page, data: users?.data });
        if (!state.totalUsers)
          commit("setTotalUsers", users?.headers["x-total-count"]);
      }
      commit("setCurrentUserList", { list: state.pages[page], page: page });
    },
    async fetchTODOs({ commit, state }, userId) {
      if (state.currentUser === userId && state.currentTODOs) return;
      commit("setCurrentTODOs", { user: null, todos: [] });
      const todos = await axios.get(
        `https://jsonplaceholder.typicode.com/todos?userId=${userId}`
      );
      commit("setCurrentTODOs", { user: userId, todos: todos?.data });
    }
  }
});
